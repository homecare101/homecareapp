require('babel-register');
import app from './src/app';
const port = process.env.PORT || 3080;

let currentApp = app;

const server = app.listen(port, function() {
    console.log('Graphql API listening on port ' + port);
});

if (module.hot) {
    module.hot.accept(['./src/app', './src/graphql/'], () => {
      server.removeListener('request', currentApp);
      server.on('request', app);
      currentApp = app;
    });
}
server.timeout = 300000;