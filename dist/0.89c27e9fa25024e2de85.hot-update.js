exports.id = 0;
exports.modules = {

/***/ "./src/app.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__("express");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_body_parser__ = __webpack_require__("body-parser");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_body_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_body_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_graphql_server_express__ = __webpack_require__("graphql-server-express");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_graphql_server_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_graphql_server_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__graphql__ = __webpack_require__("./src/graphql/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_db__ = __webpack_require__("./src/models/db.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_db___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__models_db__);







var app = __WEBPACK_IMPORTED_MODULE_0_express___default()();

app.use(__WEBPACK_IMPORTED_MODULE_1_body_parser___default.a.json());

app.use('/graphiql', Object(__WEBPACK_IMPORTED_MODULE_2_graphql_server_express__["graphiqlExpress"])({
    endpointURL: '/graphql'
}));

app.use('/graphql', Object(__WEBPACK_IMPORTED_MODULE_2_graphql_server_express__["graphqlExpress"])(function (request) {
    return {
        schema: __WEBPACK_IMPORTED_MODULE_3__graphql__["default"],
        context: request
    };
}));

/* harmony default export */ __webpack_exports__["default"] = (app);

/***/ }),

/***/ "./src/models/db.js":
/***/ (function(module, exports, __webpack_require__) {

var mongoose = __webpack_require__("mongoose");
var config = __webpack_require__("./src/config.js");
console.log(config.mongoDB);
mongoose.connect(config.mongoDB, {
    useMongoClient: true
});

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open');
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

/***/ })

};