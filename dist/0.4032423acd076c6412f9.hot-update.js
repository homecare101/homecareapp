exports.id = 0;
exports.modules = {

/***/ "./server.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_app__ = __webpack_require__("./src/app.js");
__webpack_require__("babel-register");

var port = Object({"BUILD_TARGET":"server"}).PORT || 3080;

var currentApp = __WEBPACK_IMPORTED_MODULE_0__src_app__["default"];

var server = __WEBPACK_IMPORTED_MODULE_0__src_app__["default"].listen(port, function () {
    console.log('Graphql API listening on port ' + port);
});

if (true) {
    module.hot.accept(["./src/app.js", "./src/graphql/index.js"], function(__WEBPACK_OUTDATED_DEPENDENCIES__) { /* harmony import */ __WEBPACK_IMPORTED_MODULE_0__src_app__ = __webpack_require__("./src/app.js"); (function () {
        server.removeListener('request', currentApp);
        server.on('request', __WEBPACK_IMPORTED_MODULE_0__src_app__["default"]);
        currentApp = __WEBPACK_IMPORTED_MODULE_0__src_app__["default"];
    })(__WEBPACK_OUTDATED_DEPENDENCIES__); });
}
server.timeout = 300000;

/***/ })

};