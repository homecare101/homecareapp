exports.id = 0;
exports.modules = {

/***/ "./node_modules/webpack/buildin/harmony-module.js":
false,

/***/ "./src/app.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_server_express__ = __webpack_require__("graphql-server-express");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_graphql_server_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_graphql_server_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__graphql__ = __webpack_require__("./src/graphql/index.js");
var express = __webpack_require__("express");
var bodyParser = __webpack_require__("body-parser");

var jwt = __webpack_require__("./node_modules/jsonwebtoken/index.js");


var db = __webpack_require__("./src/models/db.js");

var app = express();

app.use(bodyParser.json());

app.use('/graphiql', Object(__WEBPACK_IMPORTED_MODULE_0_graphql_server_express__["graphiqlExpress"])({
    endpointURL: '/graphql'
}));

app.use('/graphql', Object(__WEBPACK_IMPORTED_MODULE_0_graphql_server_express__["graphqlExpress"])({
    schema: __WEBPACK_IMPORTED_MODULE_1__graphql__["default"]
}));

/* harmony default export */ __webpack_exports__["default"] = (app);

/***/ })

};