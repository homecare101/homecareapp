exports.id = 0;
exports.modules = {

/***/ "./src/graphql/queries/Authentication/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__("babel-runtime/regenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator__ = __webpack_require__("babel-runtime/helpers/asyncToGenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_graphql__ = __webpack_require__("graphql");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_graphql___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_graphql__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__types_UserTypes__ = __webpack_require__("./src/graphql/types/UserTypes.js");



var jwt = __webpack_require__("./node_modules/jsonwebtoken/index.js");


var config = __webpack_require__("./src/config.js");

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'TestQuery',
    type: __WEBPACK_IMPORTED_MODULE_2_graphql__["GraphQLString"],
    description: 'Test link to check authentcation is valid through token or not.',
    resolve: function resolve(parentValues, args, context) {
        var _this = this;

        return __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default()( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
            var token, isToken;
            return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            console.log(context.headers.authorization);
                            token = context.headers.authorization;
                            _context.next = 4;
                            return jwt.verify(token, config.tokenSecret, function (err, decoded) {
                                if (err) {
                                    return false;
                                } else {
                                    // if everything is good, save to request for use in other routes
                                    //req.decoded = decoded;    
                                    //next();
                                    return true;
                                }
                            });

                        case 4:
                            isToken = _context.sent;
                            return _context.abrupt('return', isToken ? 'Successfully verified' : 'Not a valid token');

                        case 6:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this);
        }))();
    }
});

/***/ })

};