exports.id = 0;
exports.modules = {

/***/ "./src/graphql/queries/SignIn/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__("babel-runtime/regenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator__ = __webpack_require__("babel-runtime/helpers/asyncToGenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_graphql__ = __webpack_require__("graphql");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_graphql___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_graphql__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_mongoose__ = __webpack_require__("mongoose");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_mongoose___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_mongoose__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_User__ = __webpack_require__("./src/models/User.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__get_projections__ = __webpack_require__("./src/graphql/get-projections.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__types_UserTypes__ = __webpack_require__("./src/graphql/types/UserTypes.js");




var jwt = __webpack_require__("jsonwebtoken");




var config = __webpack_require__("./src/config.js");

/* harmony default export */ __webpack_exports__["a"] = ({
    name: 'SignIn',
    type: __WEBPACK_IMPORTED_MODULE_6__types_UserTypes__["a" /* default */],
    args: {
        emailId: {
            name: 'emailId',
            type: __WEBPACK_IMPORTED_MODULE_2_graphql__["GraphQLString"]
        },
        password: {
            name: 'password',
            type: __WEBPACK_IMPORTED_MODULE_2_graphql__["GraphQLString"]
        }
    },
    description: 'Users can login through this',
    resolve: function resolve(root, params, options, info) {
        var _this = this;

        return __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_asyncToGenerator___default()( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
            var projection, user, payload, token;
            return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            projection = Object(__WEBPACK_IMPORTED_MODULE_5__get_projections__["a" /* default */])(info.fieldNodes[0]);
                            _context.next = 3;
                            return __WEBPACK_IMPORTED_MODULE_4__models_User__["a" /* default */].findOne({ emailId: params.emailId, password: params.password }).select(projection).exec();

                        case 3:
                            user = _context.sent;

                            if (user) {
                                _context.next = 6;
                                break;
                            }

                            throw new Error('Invalid emailId or Password');

                        case 6:
                            console.log(user);
                            payload = {
                                userId: user._id
                            };
                            token = jwt.sign(payload, config.tokenSecret, {
                                expiresIn: config.tokenExpire // expires in 24 hours
                            });

                            user['token'] = token;
                            return _context.abrupt('return', user);

                        case 11:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this);
        }))();
    }
});

/***/ })

};