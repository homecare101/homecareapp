import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLID
} from 'graphql';

export default new GraphQLObjectType({
    name:'SignIn',
    fields:{
        _id: {
            type: new GraphQLNonNull(GraphQLID)
        },
        careType:{
            type:GraphQLString
        },
        firstName:{
            type:GraphQLString
        },
        lastName:{
            type:GraphQLString
        },
        emailId:{
            type:GraphQLString
        },
        phoneNumber:{
            type:GraphQLString
        },
        password:{
            type:GraphQLString
        },
        createdAt:{
            type:GraphQLString
        },
        updatedAt:{
            type:GraphQLString
        },
        token:{
            type:GraphQLString
        }
    }
});
