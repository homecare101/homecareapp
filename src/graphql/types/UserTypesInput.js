import {
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLID
} from 'graphql';

export default new GraphQLInputObjectType({
    name:'SignInInput',
    fields:{
        careType:{
            type:GraphQLString
        },
        firstName:{
            type:GraphQLString
        },
        lastName:{
            type:GraphQLString
        },
        emailId:{
            type:GraphQLString
        },
        phoneNumber:{
            type:GraphQLString
        },
        password:{
            type:GraphQLString
        },
    }
});
