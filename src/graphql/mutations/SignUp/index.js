import {
    GraphQLNonNull,
    GraphQLBoolean,
    GraphQLObjectType,
} from 'graphql';
import moment from 'moment';
const uuidv1 = require('uuid/v1')

import UserTypes from '../../types/UserTypes';
import UserTypesInput from '../../types/UserTypesInput';
import User from '../../../models/User';

export default {
    name:'SignUp',
    type: UserTypes,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(UserTypesInput)
        }
    },
    description:'Users can register through this',
    async resolve (root, params, options) {
        const userData = params.data;
        console.log(userData);
        userData['createdAt'] = moment();
        userData['updatedAt'] = moment();
        const userPostModel = new User(userData);
        const newUser = await userPostModel.save();
        console.log(newUser);
        if (!newUser) {
            throw new Error('Error adding new User');
        }
        return newUser;
    }
};