import {
    GraphQLObjectType,
    GraphQLSchema
} from 'graphql';

import mutations from './mutations';
import queries from './queries';

var schema = new GraphQLSchema({
    query: new GraphQLObjectType({
      name: 'RootQuery',
      fields: queries
    }),
    mutation: new GraphQLObjectType({
      name: 'RootMutation',
      fields: mutations
    })
});

export default schema;