import SignIn from './SignIn';
import Authentication from './Authentication';

export default {
    SignIn:SignIn,
    Authentication:Authentication
}
