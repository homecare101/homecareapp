import {
    GraphQLList,
    GraphQLID,
    GraphQLNonNull,
    GraphQLString,
} from 'graphql';
import {Types} from 'mongoose';
var jwt = require('jsonwebtoken');

import UserModal from '../../../models/User';
import getProjection from '../../get-projections';
import UserTypes from '../../types/UserTypes';
var config = require('../../../config');

export default {
    name:'SignIn',
    type: UserTypes,
    args: {
        emailId: {
            name: 'emailId',
            type: GraphQLString
        },
        password:{
            name:'password',
            type:GraphQLString
        }
    },
    description:'Users can login through this',
    async resolve (root, params, options,info) {
        const projection = getProjection(info.fieldNodes[0]);
        const user =  await UserModal.findOne({ emailId:params.emailId,password:params.password })
                                    .select(projection)
                                    .exec();
        if(!user){
            throw new Error('Invalid emailId or Password');
        }
        console.log(user);
        const payload = {
            userId: user._id
        } 
        var token = jwt.sign(payload,config.tokenSecret, {
            expiresIn: config.tokenExpire // expires in 24 hours
        });
        user['token'] = token;
        return user;
    }
};