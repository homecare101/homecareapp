import {
    GraphQLString
} from 'graphql';
var jwt = require('jsonwebtoken');

import UserTypes from '../../types/UserTypes';
var config = require('../../../config');

export default {
    name:'TestQuery',
    type: GraphQLString,
    description: 'Test link to check authentication is valid through token or not.',
    async resolve (parentValues, args, context) {
        console.log(context.headers.authorization);
        const token = context.headers.authorization; 
        const isToken = await jwt.verify(token, config.tokenSecret , function(err, decoded) {      
            if (err) {
                return false;    
            } else {
              // if everything is good, save to request for use in other routes
              //req.decoded = decoded;    
              //next();
              return true;
            }
        });
        return (isToken?'Successfully verified':'Not a valid token')
        // 'Not ok';
    }
};