const mongoose = require('mongoose');
const moment = require('moment');

const UserSchema = new mongoose.Schema({
    careType:String,
    firstName:String,
    lastName:String,
    emailId:{ type:String,unique:true },
    phoneNumber:String,    
    password:String,
    isEmailVerified:Boolean,
    createdAt:Date,
    updatedAt:Date,
    token:String,
});

const Users = mongoose.model('users',UserSchema);

export default Users;