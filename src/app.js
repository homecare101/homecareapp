import express from 'express'
import bodyParser from 'body-parser'
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express'
import schema from './graphql'

import db from './models/db'

const app = express();

app.use(bodyParser.json())

app.use('/graphiql',
    graphiqlExpress({
        endpointURL: '/graphql'
    })
);

app.use('/graphql', 
    graphqlExpress((request) => ({
        schema: schema,
        context:request
    }))
);

export default app;